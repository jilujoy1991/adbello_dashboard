import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
  selector: 'app-home',
  styleUrls: ['./home.component.css'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
  public date: Date = new Date();
  private mylinks: Array<any> = [];

  constructor(private breadServ: BreadcrumbService) {
    // TODO
  }

  public ngOnInit() {
    // setttings the header for the home
    this.mylinks = [
          {
            'title': 'Home',
            'icon': 'dashboard',
            'link': ['/home']
          },
          {
            'title': 'Client',
            'icon': 'usd',
            'link': ['/client']
          },
          {
            'title': 'Tables',
            'icon': 'usd',
            'link': ['/tables']
          },
          {
            'title': 'Sub menu',
            'icon': 'link',
            'sublinks': [
              {
                'title': 'Page 2',
                'link': ['/page/2'],
              },
              {
                'title': 'Page 3',
                'link': ['/page/3'],
              }
            ]
          },
          {
            'title': 'External Link',
            'icon': 'google',
            'link': ['http://google.com'],
            'external': true,
            'target': '_blank'
          },
          {
            'title': 'External Links',
            'icon': 'link',
            'sublinks': [
              {
                'title': 'Github',
                'link': ['http://github.com'],
                'icon': 'github',
                'external': true,
                'target': '_blank'
              },
              {
                'title': 'Yahoo',
                'link': ['http://yahoo.com'],
                'icon': 'yahoo',
                'external': true,
                'target': '_blank'
              }
            ]
          }
        ];


    this.breadServ.set({
      description: 'HomePage',
      display: true,
      header: 'Dashboard',
      levels: [
        {
          icon: 'dashboard',
          link: ['/home'],
          title: 'Home'
        }
      ]
    });
  }

  public ngOnDestroy() {
    // removing the header
    this.breadServ.clear();
  }

}

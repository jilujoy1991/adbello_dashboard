import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateGuard } from './services/guard.service';

// Components
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PageNumComponent } from './pages/page-num/page-num.component';
import { ClientComponent } from './pages/client/client.component';
import { TablesComponent } from './pages/tables/tables.component';

const routes: Routes = [
  // Root
  {
    component: LoginComponent,
    path: ''
  },
  {
    component: LoginComponent,
    path: 'login'
  },
  {
    canActivate: [CanActivateGuard],
    component: HomeComponent,
    path: 'home'
  },
  {
    canActivate: [CanActivateGuard],
    component: PageNumComponent,
    path: 'page/:id'
  },
  {
    canActivate: [CanActivateGuard],
    component: ClientComponent,
    path: 'client'
  },
  {
    canActivate: [CanActivateGuard],
    component: TablesComponent,
    path: 'tables'
  },
  { path: '**', redirectTo: '' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

import { Injectable } from '@angular/core';
import { Configuration } from '../app.constants';

@Injectable()
export class TabledataService {
  public modelName: string;

  constructor(private config: Configuration) { }

  private getUrl() {
      return this.config.serverWithApiUrl + '/api/getAll'+ this.modelName + '/';
    }

}

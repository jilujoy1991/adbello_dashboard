import { Component, OnInit } from '@angular/core';
import { BreadcrumbService } from '../../services/breadcrumb.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
  private mylinks: Array<any> = [];

  constructor(private breadServ: BreadcrumbService) { }

  ngOnInit() {
    this.mylinks = [
          {
            'title': 'Home',
            'icon': 'dashboard',
            'link': ['/home']
          },
          {
            'title': 'Client',
            'icon': 'usd',
            'link': ['/client']
          },
          {
            'title': 'Tables',
            'icon': 'usd',
            'link': ['/tables']
          },
          {
            'title': 'Sub menu',
            'icon': 'link',
            'sublinks': [
              {
                'title': 'Page 2',
                'link': ['/page/2'],
              },
              {
                'title': 'Page 3',
                'link': ['/page/3'],
              }
            ]
          },
          {
            'title': 'External Link',
            'icon': 'google',
            'link': ['http://google.com'],
            'external': true,
            'target': '_blank'
          },
          {
            'title': 'External Links',
            'icon': 'link',
            'sublinks': [
              {
                'title': 'Github',
                'link': ['http://github.com'],
                'icon': 'github',
                'external': true,
                'target': '_blank'
              },
              {
                'title': 'Yahoo',
                'link': ['http://yahoo.com'],
                'icon': 'yahoo',
                'external': true,
                'target': '_blank'
              }
            ]
          }
        ];

    this.breadServ.set({
      description: 'AdBello Tables',
      display: true,
      levels: [
        {
          icon: 'dashboard',
          link: ['/home'],
          title: 'Home'
        },
        {
          icon: 'clock-o',
          link: ['/tables'],
          title: 'Tables'
        }
      ]
    });
  }

}

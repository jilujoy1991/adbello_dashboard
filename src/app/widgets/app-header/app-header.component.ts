import { Component, Input } from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component( {
    selector: 'app-header',
    styleUrls: ['./app-header.component.css'],
    templateUrl: './app-header.component.html'
})
export class AppHeaderComponent {

  constructor(private auth: AuthService, private router: Router) {
    // TODO
  }

  logout() {
    console.log('clicked on logout');
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}

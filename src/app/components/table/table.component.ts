import { Component, OnInit } from '@angular/core';
import { TableData } from './table-data';
import { Http, Headers, Response } from '@angular/http';
import { Configuration } from '../../app.constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public Tabledata:any;
  public table:string = 'Login';
  public datatable:boolean = false;

  public rows:Array<any> = [];
  public columnNames:Array<any> = [];
  public columns:Array<any> = [
    {title: 'Name', name: 'name', filtering: {filterString: '', placeholder: 'Filter by name'}, sort: 'asc'},
    {
      title: 'Position',
      name: 'position',
      sort: 'asc',
      filtering: {filterString: '', placeholder: 'Filter by position'}
    },
    {title: 'Office', className: ['office-header', 'text-success'], name: 'office', sort: 'asc', filtering: {filterString: '', placeholder: 'Filter by Office'} },
    {title: 'Extn.', name: 'ext', filtering: {filterString: '', placeholder: 'Filter by extn.'}, sort: 'asc'},
    {title: 'Start date', className: 'text-warning', name: 'startDate', sort: 'asc'},
    {title: 'Salary ($)', name: 'salary'  , sort: 'asc'}
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered']
  };

  private data:Array<any> = TableData;

  public constructor( private http: Http, private router: Router) {
    this.length = this.data.length;
    
  }

  public ngOnInit():void {
    //this.onChangeTable(this.config);
    this.load('Login');
  }

  public load(val){
    this.table = val;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get(Configuration.API_ENDPOINT + '/api/admin/tables/' + val, { headers: headers })
        .subscribe(
        data => {
          let res = data.json();
          this.Tabledata = res.data;
          this.setData();        
        },
        err => {
          console.log(err.json());
          this.Tabledata = [];
        }
        );
  }

  setData(){
    let columnNames = [];
    var columnsIn = this.Tabledata[0]; 
    for(var key in columnsIn){
        columnNames.push(key); // here is your column name you are looking for
    }
    this.columns = [];
    this.columnNames = columnNames;
    for (let i=0; i < columnNames.length; i++){
      let title = columnNames[i];
      let placeholder = 'Filter by ' + title ;
      let value = {title: title, name: columnNames[i], filtering: {filterString: '', placeholder: placeholder }, sort: 'asc', className: 'text-success',}
      this.columns.push(value);
    }
    this.data = [];
    this.data = this.Tabledata;
    this.config =  {
        paging: true,
        sorting: {columns: this.columns},
        filtering: {filterString: ''},
        className: ['table-striped', 'table-bordered']
    };
    this.length = this.data.length;
    this.onChangeTable(this.config);
  }
  
  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }
  
  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          //console.log('values' + typeof(item[column.name]) + ' haii ' + item[column.name]);
           item[column.name] = item[column.name].toString();
           return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];
    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
     //Object.assign(this.config.filtering, config.filtering);
     this.config.filtering = config.filtering ;
    }

    if (config.sorting) {
      //Object.assign(this.config.sorting, config.sorting);
      this.config.sorting = config.sorting ;
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    console.log(this.rows);
    console.log(this.columns);
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data);
}

}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class CanActivateGuard implements CanActivate {
  constructor(private router: Router) { }

  public canActivate() {
    // test here if you user is logged
    if (localStorage.getItem('currentUser')) {
      // logged in so return true
        return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}

import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public server: string = 'http://test.weberantoine.fr/';
    public apiUrl: string = 'php-crud-api/api.php/';
    //public APIUrl: string = 'http://ec2-52-25-90-93.us-west-2.compute.amazonaws.com:4000';
    public APIUrl: string = 'http://localhost:4000';
    public serverWithApiUrl = this.server + this.apiUrl;
    public static get API_ENDPOINT(): string { return 'http://ec2-52-25-90-93.us-west-2.compute.amazonaws.com:4000'; }
    //public static get API_ENDPOINT(): string { return 'http://localhost:4000'; }
}

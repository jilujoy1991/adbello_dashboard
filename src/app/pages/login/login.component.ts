import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Http, Headers, Response } from '@angular/http';
import { Configuration } from '../../app.constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  message: string;

  constructor(private route: ActivatedRoute, private auth: AuthService, 
        private router: Router, private http: Http) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home';
  }
  login(){
    this.model.type = 'admin';
    //this.router.navigate([this.returnUrl]);
    //this.auth.login(value); 
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.http.post(Configuration.API_ENDPOINT + '/api/login', JSON.stringify(this.model), { headers: headers })
        .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
          let res = data.json();
          this.auth.login(res);
        },
        err => {
          console.log(err.json()); 
          let res = err.json();
          this.message = res.message;
        }
        );           
  }

}
